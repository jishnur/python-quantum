---
theme : "night"
transition: "slide"
highlightTheme: "monokai"
slideNumber: false
title: "Quantum computation with QisKit"
---

## Quantum computation 

---

## QisKit

- [Qiskit](https://qiskit.org/)

---

### Starting with a virtual environment

python3 -m venv \path\to\virtual\environment

---
